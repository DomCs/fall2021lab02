// Domenico Cuscuna 2038364

public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;


    //Constructor
    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    //Get Methods
    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    //Set Methods
    public void setManufacturer(String manufacturer){
        this.manufacturer = manufacturer;
    }
    public void setNumberGears(int numberGears){
        this.numberGears = numberGears;
    }
    public void setMaxSpeed(double maxSpeed){
        this.maxSpeed = maxSpeed;
    }
    
    //ToString() Method
    public String toString(){
        return "Manufacturer "+this.manufacturer+", Number of Gears "+
        this.numberGears+", Max Speed "+this.maxSpeed;
    }
}