// Domenico Cuscuna 2038364

public class BikeStore {
    public static void main (String args[]){
        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("GreenBikeCompany",20,30);
        bikes[1] = new Bicycle("BlueBikeCompany",10,100);
        bikes[2] = new Bicycle("YellowBikeCompany",200,40);
        bikes[3] = new Bicycle("RedBikeCompany",90,90);

        for(int i=0;i<bikes.length;i++){
            System.out.println(bikes[i]);
        }
    }
    
}
